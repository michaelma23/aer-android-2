package au.com.mstech.ausexchangerate.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import au.com.mstech.ausexchangerate.R;
import au.com.mstech.ausexchangerate.db.ExchangeRateDbAdapter;
import au.com.mstech.ausexchangerate.model.Currency;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mma
 * Date: 7/10/13
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencySearchActivity extends ListActivity {

    private List<Currency> currencyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_view);

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third - the Array of data

        ExchangeRateDbAdapter dbAdapter = new ExchangeRateDbAdapter(this);
        dbAdapter.open();
        currencyList = dbAdapter.getAllCurrencyList();
        List<String> currencyDescList = new ArrayList<String>();
        for(Currency currency : currencyList){
            currencyDescList.add(currency.getDescription());
        }

        ArrayAdapter <String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,currencyDescList);

        dbAdapter.close();

        // Assign adapter to List
        setListAdapter(adapter);
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        Currency selectedCurrency = currencyList.get(position);
        setResult(RESULT_OK,new Intent().putExtra("CurrencyCode",selectedCurrency.getCode()));
        finish();
    }
}
