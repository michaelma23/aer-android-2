package au.com.mstech.ausexchangerate.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.*;
import android.widget.*;
import au.com.mstech.ausexchangerate.R;
import au.com.mstech.ausexchangerate.db.ExchangeRateDbAdapter;
import au.com.mstech.ausexchangerate.model.BankCode;
import au.com.mstech.ausexchangerate.model.Rate;
import au.com.mstech.ausexchangerate.adapter.ExRateListAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AusExchangeRateActivity extends Activity {
    public static ExchangeRateDbAdapter dbAdapter = null;
    private static final int CURRENCY_SELECTION_REQUEST = 0;
    private Menu menu;
    private SimpleDateFormat dateFormat;
    AsyncHttpClient requestClient;
    ExchangeRateHandler exchangeRateHandler;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        init();
    }

    public void showExRatesDownloadErrorAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.downloadError)
                .setNegativeButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            requestExchangeRates();
                        } catch (Exception e) {
                            showExRatesDownloadErrorAlert();
                        }
                    }
                })
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        builder.create().show();
    }

    private void init(){
        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        final Button button = (Button) findViewById(R.id.currencyButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            // Perform action on click
            Intent intent = new Intent(AusExchangeRateActivity.this, CurrencySearchActivity.class);
            startActivityForResult(intent, CURRENCY_SELECTION_REQUEST);
            }
        });

        if(pref.contains("Currency")){
            button.setText(pref.getString("Currency","USD"));
        }

        Spinner rateTypeView = (Spinner) findViewById(R.id.rate_type);
        ArrayAdapter<CharSequence> rateTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.rate_type, android.R.layout.simple_spinner_item);
        rateTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rateTypeView.setAdapter(rateTypeAdapter);
        rateTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                refreshPage();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        if(pref.contains("RateType")){
            String savedRateType = pref.getString("RateType", null);
            if(savedRateType != null){
                for(int i = 0; i < rateTypeView.getCount(); i++){
                    if(savedRateType.equalsIgnoreCase((String)rateTypeView.getItemAtPosition(i))){
                        rateTypeView.setSelection(i);
                        break;
                    }
                }
            }
        }


        dbAdapter = new ExchangeRateDbAdapter(this).open();
        try{

            long lastUpdateDate = pref.getLong("LastUpdateDate",0);
            if(lastUpdateDate == 0 || (new Date().getTime() - lastUpdateDate) > (1000 * 60 * 60) ){
                requestExchangeRates();
            }
            else{
                refreshPage();
            }
        }
        catch (Exception e){
            e.printStackTrace();
            showExRatesDownloadErrorAlert();
        }
    }

    private void setEnableUI(boolean enable){
        Button currencyButton = (Button) findViewById(R.id.currencyButton);
        currencyButton.setEnabled(enable);
        Spinner rateTypeSpinner = (Spinner) findViewById(R.id.rate_type);
        rateTypeSpinner.setEnabled(enable);
        if(menu != null && menu.size() > 0){
            menu.getItem(0).setEnabled(enable);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if(item.getItemId() == R.id.refreshMenu){
            try {
                requestExchangeRates();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                showExRatesDownloadErrorAlert();
            }
        }
        return false;
    }

    private void cancelAllRequests(){
        if(requestClient != null)
            requestClient.cancelRequests(this, true);
        if(exchangeRateHandler != null)
            exchangeRateHandler.cancel(true);
        showProgress(false);
        requestClient = null;
        exchangeRateHandler = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            // start new Activity here
            if(requestClient != null || exchangeRateHandler != null){
                cancelAllRequests();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        final Button currencyButton = (Button) findViewById(R.id.currencyButton);
        if(currencyButton != null){
            pref.edit().putString("Currency",currencyButton.getText().toString()).apply();
        }

        Spinner rateTypeView = (Spinner) findViewById(R.id.rate_type);
        if(rateTypeView != null){
            pref.edit().putString("RateType", (String)rateTypeView.getSelectedItem()).apply();
        }
        cancelAllRequests();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
//        dbAdapter.close();
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data){
        if(requestCode == CURRENCY_SELECTION_REQUEST && resultCode == RESULT_OK){
            Button currencyButton = (Button) findViewById(R.id.currencyButton);
            currencyButton.setText(data.getStringExtra("CurrencyCode"));
            refreshPage();
        }
    }

    public void showProgress(boolean show){
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar);

        setEnableUI(!show);
        if(show){
            progressBar.setVisibility(ProgressBar.VISIBLE);
        }
        else{
            progressBar.setVisibility(ProgressBar.INVISIBLE);
        }
    }

    public void refreshPage(){

        String currencyCode = ((Button) findViewById(R.id.currencyButton)).getText().toString();
        List<Rate> list = dbAdapter.getExRatesList(currencyCode);

        Spinner rateTypeView = (Spinner) findViewById(R.id.rate_type);
        String rateType = (String) rateTypeView.getSelectedItem();

        ListView rateListView = (ListView) findViewById(R.id.rate_list);
        ListAdapter adapter = rateListView.getAdapter();
        if(adapter == null){
            adapter = new ExRateListAdapter(this, R.layout.ex_rate_list_item,  list, currencyCode, rateType);
            ArrayList<ListView.FixedViewInfo> headerList = new ArrayList<ListView.FixedViewInfo>();

            LayoutInflater inflater = getLayoutInflater();
            ViewGroup header = (ViewGroup)inflater.inflate(R.layout.ex_rate_list_header, rateListView, false);

            ListView.FixedViewInfo headerViewInfo = rateListView.new FixedViewInfo();
            headerViewInfo.isSelectable = false;
            headerViewInfo.view = header;

            headerList.add(headerViewInfo);

            HeaderViewListAdapter adapterWithHeader = new HeaderViewListAdapter(headerList, null, adapter);

            rateListView.setAdapter(adapterWithHeader);
        }
        else{
            ExRateListAdapter exRateAdapter = (ExRateListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
            exRateAdapter.setCurrencyCode(currencyCode);
            exRateAdapter.setData(list,rateType);
            exRateAdapter.notifyDataSetChanged();
        }
        Date lastUpdateDate = dbAdapter.lastUpdateDate();
        if(lastUpdateDate != null){
            TextView updateDateText = (TextView) findViewById(R.id.updateDateText);
            updateDateText.setText(getString(R.string.updateDate, dateFormat.format(lastUpdateDate)));
        }
    }

    protected void requestExchangeRates() throws JSONException, UnsupportedEncodingException {

        cancelAllRequests();
        requestClient = new AsyncHttpClient();
        JSONObject requestJson = new JSONObject();
        requestJson.put("type","get-exchange-rate");
        requestJson.put("properties",new JSONObject());
        ByteArrayEntity entity;
        entity = new ByteArrayEntity(requestJson.toString().getBytes("UTF-8"));
        requestClient.post(this, "http://aer-mstech.elasticbeanstalk.com/api", entity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(org.json.JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                if(requestClient != null){
                    if(exchangeRateHandler != null){
                        exchangeRateHandler.cancel(true);
                    }
                    exchangeRateHandler = new ExchangeRateHandler();
                    exchangeRateHandler.execute(jsonObject);
                }
            }

            @Override
            public void onFailure(java.lang.Throwable throwable, java.lang.String s) {
                super.onFailure(throwable, s);
                showProgress(false);
                showExRatesDownloadErrorAlert();
            }

            @Override
            public void onFinish() {
                super.onFinish();    //To change body of overridden methods use File | Settings | File Templates.
                requestClient = null;
            }



            @Override
            public void onStart() {
                showProgress(true);
            }
        });

    }

    private class ExchangeRateHandler extends AsyncTask {

        @Override
        protected Boolean doInBackground(Object... objects) {
            if(!isCancelled() && objects.length > 0){
                try {
                    saveExchangeRates((JSONObject)objects[0]);
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AusExchangeRateActivity.this);
                    pref.edit().putLong("LastUpdateDate", new Date().getTime()).apply();
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    return false;
                }
            }
            return false;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);    //To change body of overridden methods use File | Settings | File Templates.
            showProgress(false);
            if(!isCancelled()){
                if(o != null && (Boolean) o){
                    refreshPage();
                }
                else{
                    showExRatesDownloadErrorAlert();
                }
            }
            exchangeRateHandler = null;
        }

        private void saveExchangeRates(JSONObject jsonObject) throws JSONException{
            try {
                saveExchangeRates(parseExchangeRateJson(jsonObject));
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                throw e;
            }
        }

        private void saveExchangeRates(List<Rate> rates){
            AusExchangeRateActivity.dbAdapter.updateExchangeRate(rates);
        }

        private String getSafeJsonString(JSONObject jsonObject, String tag) throws JSONException{
            if(jsonObject.has(tag)){
                return jsonObject.getString(tag);
            }
            return "";
        }

        private double getSafeJsonDouble(JSONObject jsonObject, String tag) throws JSONException{
            if(jsonObject.has(tag)){
                return jsonObject.getDouble(tag);
            }
            return 0;
        }

        private List<Rate> parseExchangeRateJson(JSONObject jsonObject) throws JSONException{
            List<Rate> rates = new ArrayList<Rate>();
            if(jsonObject != null && jsonObject.has("code") && jsonObject.getInt("code") == 0){
                JSONArray bankArray = jsonObject.getJSONArray("ExchangeRates");
                for(int i = 0; i < bankArray.length(); i++){
                    JSONObject bankObject = bankArray.getJSONObject(i);
                    String bankName = bankObject.getString("bankName");
                    BankCode bankCode = BankCode.valueOf(bankObject.getString("bankCode"));
                    java.util.Date updateDate = new Date(bankObject.getLong("updateDate"));
                    JSONArray ratesArray = bankObject.getJSONArray("rate");
                    for(int j = 0; j < ratesArray.length(); j++){
                        JSONObject rateObject = ratesArray.getJSONObject(j);
                        double sellCash = getSafeJsonDouble(rateObject, "sellCash");
                        double buyTcard = getSafeJsonDouble(rateObject, "buyTcard");
                        String currencyCode = getSafeJsonString(rateObject, "currencyCode");
                        double sellDraft = getSafeJsonDouble(rateObject, "sellDraft");
                        double buyTcheque = getSafeJsonDouble(rateObject, "buyTcheque");
                        double sellTT = getSafeJsonDouble(rateObject, "sellTT");
                        double buyFcheque = getSafeJsonDouble(rateObject, "buyFcheque");
                        double buyDraft = getSafeJsonDouble(rateObject, "buyDraft");
                        double buyTT = getSafeJsonDouble(rateObject, "buyTT");
                        double buyCash = getSafeJsonDouble(rateObject, "buyCash");
                        String currencyName = getSafeJsonString(rateObject, "currencyName");
                        double sellTcard = getSafeJsonDouble(rateObject, "sellTcard");
                        double sellTcheque = getSafeJsonDouble(rateObject, "sellTcheque");
                        double sellFcheque = getSafeJsonDouble(rateObject, "sellFcheque");
                        rates.add(new Rate(bankCode,bankName,currencyCode,currencyName,buyTcheque,buyFcheque,buyTcard,buyTT,buyCash,buyDraft,sellTcheque,sellFcheque,sellTcard,sellTT,sellCash,sellDraft,updateDate));
                    }
                }
            }
            return rates;
        }
    }
}