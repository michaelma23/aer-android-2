package au.com.mstech.ausexchangerate.model;

public enum BankCode {
	CBA,
	HSBC,
	ANZ,
	NAB,
	STG,
	WBC,
	BWA;
}
