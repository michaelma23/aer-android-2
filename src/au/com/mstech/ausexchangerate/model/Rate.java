package au.com.mstech.ausexchangerate.model;

import java.util.Date;

public class Rate {
    private BankCode bankCode;
    private String bankName;
    private String code;
    private String name;
    private double buyTcheque;
    private double buyFcheque;
    private double buyTcard;
    private double buyTT;
    private double buyCash;
    private double buyDraft;
    private double sellTcheque;
    private double sellFcheque;
    private double sellTcard;
    private double sellTT;
    private double sellCash;
    private double sellDraft;
    private Date updateDate;

    public Rate(BankCode bankCode, String bankName, String code, String name, double buyTcheque, double buyFcheque, double buyTcard, double buyTT, double buyCash, double buyDraft, double sellTcheque, double sellFcheque, double sellTcard, double sellTT, double sellCash, double sellDraft, Date updateDate) {
        this.setBankName(bankName);
        this.setBankCode(bankCode);
        this.setCurrencyCode(code);
        this.setCurrencyName(name);
        this.setBuyTcheque(buyTcheque);
        this.setBuyFcheque(buyFcheque);
        this.setBuyTcard(buyTcard);
        this.setBuyTT(buyTT);
        this.setBuyCash(buyCash);
        this.setBuyDraft(buyDraft);
        this.setSellTcheque(sellTcheque);
        this.setSellFcheque(sellFcheque);
        this.setSellTcard(sellTcard);
        this.setSellTT(sellTT);
        this.setSellCash(sellCash);
        this.setSellDraft(sellDraft);
        this.setUpdateDate(updateDate);
    }

    public Rate() {
        //To change body of created methods use File | Settings | File Templates.
    }

    @Override
	public boolean equals(Object obj){
		if(!(obj instanceof Rate)){
			return false;
		}
		Rate rate = (Rate) obj;
		
		return rate.getCode().equals(getCode());
	}
	
	@Override
	public int hashCode(){
		return getCode().hashCode();
	}

    public String getCode() {
        return code;
    }

    public void setCurrencyCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setCurrencyName(String name) {
        this.name = name;
    }

    public double getBuyTcheque() {
        return buyTcheque;
    }

    public void setBuyTcheque(double buyTcheque) {
        this.buyTcheque = buyTcheque;
    }

    public double getBuyFcheque() {
        return buyFcheque;
    }

    public void setBuyFcheque(double buyFcheque) {
        this.buyFcheque = buyFcheque;
    }

    public double getBuyTcard() {
        return buyTcard;
    }

    public void setBuyTcard(double buyTcard) {
        this.buyTcard = buyTcard;
    }

    public double getBuyTT() {
        return buyTT;
    }

    public void setBuyTT(double buyTT) {
        this.buyTT = buyTT;
    }

    public double getBuyCash() {
        return buyCash;
    }

    public void setBuyCash(double buyCash) {
        this.buyCash = buyCash;
    }

    public double getBuyDraft() {
        return buyDraft;
    }

    public void setBuyDraft(double buyDraft) {
        this.buyDraft = buyDraft;
    }

    public double getSellTcheque() {
        return sellTcheque;
    }

    public void setSellTcheque(double sellTcheque) {
        this.sellTcheque = sellTcheque;
    }

    public double getSellFcheque() {
        return sellFcheque;
    }

    public void setSellFcheque(double sellFcheque) {
        this.sellFcheque = sellFcheque;
    }

    public double getSellTcard() {
        return sellTcard;
    }

    public void setSellTcard(double sellTcard) {
        this.sellTcard = sellTcard;
    }

    public double getSellTT() {
        return sellTT;
    }

    public void setSellTT(double sellTT) {
        this.sellTT = sellTT;
    }

    public double getSellCash() {
        return sellCash;
    }

    public void setSellCash(double sellCash) {
        this.sellCash = sellCash;
    }

    public double getSellDraft() {
        return sellDraft;
    }

    public void setSellDraft(double sellDraft) {
        this.sellDraft = sellDraft;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public BankCode getBankCode() {
        return bankCode;
    }

    public void setBankCode(BankCode bankCode) {
        this.bankCode = bankCode;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "bankCode=" + bankCode +
                ", bankName='" + bankName + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", buyTcheque=" + buyTcheque +
                ", buyFcheque=" + buyFcheque +
                ", buyTcard=" + buyTcard +
                ", buyTT=" + buyTT +
                ", buyCash=" + buyCash +
                ", buyDraft=" + buyDraft +
                ", sellTcheque=" + sellTcheque +
                ", sellFcheque=" + sellFcheque +
                ", sellTcard=" + sellTcard +
                ", sellTT=" + sellTT +
                ", sellCash=" + sellCash +
                ", sellDraft=" + sellDraft +
                ", updateDate=" + updateDate +
                '}';
    }
}
