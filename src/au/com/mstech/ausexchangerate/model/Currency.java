package au.com.mstech.ausexchangerate.model;

/**
 * Created with IntelliJ IDEA.
 * User: mma
 * Date: 7/10/13
 * Time: 3:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Currency {
    private String code;
    private String description;

    public Currency(String code, String description) {
        this.setCode(code);
        this.setDescription(description);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
