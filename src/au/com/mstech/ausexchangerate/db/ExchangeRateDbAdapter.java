package au.com.mstech.ausexchangerate.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import au.com.mstech.ausexchangerate.model.BankCode;
import au.com.mstech.ausexchangerate.model.Currency;
import au.com.mstech.ausexchangerate.model.Rate;

public class ExchangeRateDbAdapter {
	private static final String DATABASE_NAME = "AER";
	private static final String DATABASE_TABLE_EXCHANGE_RATES = "exchange_rates";
    private static final String DATABASE_TABLE_CURRENCY_INFO = "currency_info";
    private static final String KEY_BANK_CODE = "bankCode";
    private static final String KEY_BANK_NAME = "bankName";
	private static final String KEY_CURRENCY_CODE = "currencyCode";
	private static final String KEY_CURRENCY_NAME = "currencyName";
    private static final String KEY_CURRENCY_DESC = "currencyDesc";
	private static final String KEY_BUY_TCHEQUE = "buyTcheque";
    private static final String KEY_BUY_FCHEQUE = "buyFcheque";
	private static final String KEY_BUY_TCARD = "buyTcard";
	private static final String KEY_BUY_TT = "buyTT";
	private static final String KEY_BUY_CASH = "buyCash";
    private static final String KEY_BUY_DRAFT = "buyDraft";
	private static final String KEY_SELL_TCHEQUE= "sellTcheque";
    private static final String KEY_SELL_FCHEQUE= "sellFcheque";
	private static final String KEY_SELL_TCARD = "sellTcard";
	private static final String KEY_SELL_TT = "sellTT";
	private static final String KEY_SELL_CASH = "sellCash";
	private static final String KEY_SELL_DRAFT = "sellDraft";
	private static final String KEY_UPDATE_DATE = "updateDate";
	private static final int DATABASE_VERSION = 1;
	private static final String[] TABLES_CREATE = new String[]
                                { "CREATE TABLE "+
                                    DATABASE_TABLE_EXCHANGE_RATES + " ( " +
                                        KEY_BANK_CODE + " TEXT, " +
                                        KEY_BANK_NAME + " TEXT, " +
                                        KEY_CURRENCY_CODE + " TEXT, " +
                                        KEY_CURRENCY_NAME + " TEXT, " +
                                        KEY_BUY_TCHEQUE + " REAL, " +
                                        KEY_BUY_FCHEQUE + " REAL, " +
                                        KEY_BUY_TCARD + " REAL, " +
                                        KEY_BUY_TT + " REAL, " +
                                        KEY_BUY_CASH + " REAL, " +
                                        KEY_BUY_DRAFT + " REAL, " +
                                        KEY_SELL_TCHEQUE + " REAL, " +
                                        KEY_SELL_FCHEQUE + " REAL, " +
                                        KEY_SELL_TCARD + " REAL, " +
                                        KEY_SELL_TT + " REAL, " +
                                        KEY_SELL_CASH + " REAL, " +
                                        KEY_SELL_DRAFT + " REAL, " +
                                        KEY_UPDATE_DATE + " NUMERIC, " +
                                    "PRIMARY KEY ("+KEY_BANK_CODE+" , "+ KEY_CURRENCY_CODE+"))",

                                    "CREATE TABLE " +
                                     DATABASE_TABLE_CURRENCY_INFO  + " ( " +
                                        KEY_CURRENCY_CODE + " TEXT, " +
                                        KEY_CURRENCY_DESC + " TEXT, " +
                                    "PRIMARY KEY ("+KEY_CURRENCY_CODE +"))"
                                };
    private static final String[] INDEXES_CREATE = new String[]{
            "CREATE INDEX IF NOT EXISTS CURRENCY_CODE_IDX ON " + DATABASE_TABLE_EXCHANGE_RATES + " ( "+KEY_CURRENCY_CODE+" ) "
    };
    private static final String[] DATA_PRELOAD = new String[]{
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"USD\" , \"USD - UNITED STATES DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"EUR\" , \"EUR - EURO\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"GBP\" , \"GBP - GREAT BRITISH POUND\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"INR\" , \"INR - INDIAN RUPEE\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"CAD\" , \"CAD - CANADIAN DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"ZAR\" , \"ZAR - SOUTH AFRICAN RAND\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"NZD\" , \"NZD - NEW ZEALAND DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"JPY\" , \"JPY - JAPANESE YEN\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"CNY\" , \"CNY - CHINESE RENMINBI\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"HKD\" , \"HKD - HONG KONG DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"AED\" , \"AED - UNITED ARAB EMIRATES\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"ARS\" , \"ARS - ARGENTINA PESO\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BBD\" , \"BBD - BARBADOS DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BDT\" , \"BDT - BANGLADESHI TAKA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BHD\" , \"BHD - BAHRAIN DINAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BMD\" , \"BMD - BERMUDA DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BND\" , \"BND - BRUNEI DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BRL\" , \"BRL - BRAZILIAN REAL\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"BSD\" , \"BSD - BAHAMAS DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"CHF\" , \"CHF - SWISS FRANC\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"CLP\" , \"CLP - CHILEAN PESO\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"CZK\" , \"CZK - CZECH KORUNA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"DKK\" , \"DKK - DANISH KRONER\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"EGP\" , \"EGP - EGYPTIAN POUND\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"FJD\" , \"FJD - FIJIAN DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"HRK\" , \"HRK - CROATIAN KUNA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"HUF\" , \"HUF - HUNGARIAN FORINT\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"IDR\" , \"IDR - INDONESIAN RUPIAH\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"ILS\" , \"ILS - ISRAEL SHEKEL\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"JOD\" , \"JOD - JORDAN DINAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"KES\" , \"KES - KENYAN SHILLING\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"KRW\" , \"KRW - SOUTH KOREAN WON\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"KWD\" , \"KWD - KUWAITI DINARS\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"LBP\" , \"LBP - LEBANESE POUND\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"LKR\" , \"LKR - SRI LANKA RUPEE\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"MTL\" , \"MTL - MALTESE LIRA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"MUR\" , \"MUR - MAURITIUS RUPEE\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"MXN\" , \"MXN - MEXICAN PESO\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"MYR\" , \"MYR - MALAYSIAN RINGGIT\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"NOK\" , \"NOK - NORWEGIAN KRONER\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"OMR\" , \"OMR - OMAN RIAL\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"PGK\" , \"PGK - PAPUA NEW GUINEA KINA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"PHP\" , \"PHP - PHILIPPINE PESO\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"PKR\" , \"PKR - PAKISTAN RUPEE\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"PLN\" , \"PLN - POLISH ZLOTY\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"QAR\" , \"QAR - QATAR RIYAL\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"RON\" , \"RON - ROMANIAN NEW LEU\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"SAR\" , \"SAR - SAUDI ARABIA RIYAL\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"SBD\" , \"SBD - SOLOMON ISLAND DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"SEK\" , \"SEK - SWEDISH KRONOR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"SGD\" , \"SGD - SINGAPORE DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"THB\" , \"THB - THAILAND BAHT\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"TOP\" , \"TOP - TONGA PAANGA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"TRY\" , \"TRY - TURKISH LIRA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"TWD\" , \"TWD - NEW TAIWAN DOLLAR\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"VND\" , \"VND - VIETNAMESE DONG\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"VUV\" , \"VUV - VANUATU VATU\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"WST\" , \"WST - WESTERN SAMOA TALA\")",
            "INSERT INTO " + DATABASE_TABLE_CURRENCY_INFO + " VALUES ( \"XPF\" , \"XPF - FRENCH PACIFIC FRANC\")"
    };
	private final Context mCtx;
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	public ExchangeRateDbAdapter(Context ctx) {
		this.mCtx = ctx;
	}
			
	public ExchangeRateDbAdapter open() throws android.database.SQLException {

		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		mDbHelper.close();
	}
	
	public void updateExchangeRate(List<Rate> rates) throws SQLException{
		if(rates != null && rates.size() > 0){
			clearBankRates();
			ContentValues values = new ContentValues();
			for(Rate rate : rates){
				values.clear();
                values.put(KEY_BANK_CODE, rate.getBankCode().toString());
                values.put(KEY_BANK_NAME, rate.getBankName());
                values.put(KEY_CURRENCY_CODE, rate.getCode());
                values.put(KEY_CURRENCY_NAME, rate.getName());
                values.put(KEY_BUY_TCHEQUE, rate.getBuyTcheque());
                values.put(KEY_BUY_FCHEQUE, rate.getBuyFcheque());
                values.put(KEY_BUY_TCARD, rate.getBuyTcard());
                values.put(KEY_BUY_TT, rate.getBuyTT());
                values.put(KEY_BUY_CASH, rate.getBuyCash());
                values.put(KEY_BUY_DRAFT, rate.getBuyDraft());
                values.put(KEY_SELL_TCHEQUE, rate.getSellTcheque());
                values.put(KEY_SELL_FCHEQUE, rate.getSellFcheque());
                values.put(KEY_SELL_TCARD, rate.getSellTcard());
                values.put(KEY_SELL_TT, rate.getSellTT());
                values.put(KEY_SELL_CASH, rate.getSellCash());
                values.put(KEY_SELL_DRAFT, rate.getSellDraft());
                values.put(KEY_UPDATE_DATE, rate.getUpdateDate().getTime());
				try{
                    mDb.insertOrThrow(DATABASE_TABLE_EXCHANGE_RATES, null, values);
				}
				catch (SQLException e) {
                    e.printStackTrace();
					throw e;
				}
			}
		}
	}
	
    public Cursor getAllCurrency(){

        return mDb.query(DATABASE_TABLE_CURRENCY_INFO, new String[]{KEY_CURRENCY_CODE,KEY_CURRENCY_DESC},
                null,null, null, null, null);
    }

    public Cursor getAllRates(){

        return mDb.query(DATABASE_TABLE_EXCHANGE_RATES, new String[]{KEY_BANK_CODE, KEY_BANK_NAME, KEY_CURRENCY_CODE, KEY_CURRENCY_NAME,
                KEY_BUY_TCHEQUE, KEY_BUY_FCHEQUE, KEY_BUY_TCARD, KEY_BUY_TT, KEY_BUY_CASH, KEY_BUY_DRAFT, KEY_SELL_TCHEQUE,
                KEY_SELL_FCHEQUE, KEY_SELL_TCARD, KEY_SELL_TT, KEY_SELL_CASH, KEY_SELL_DRAFT, KEY_UPDATE_DATE},
                null,null, null, null, null);
    }

	public Cursor getExchangeRate(String currency_code){
		return mDb.query(DATABASE_TABLE_EXCHANGE_RATES, new String[]{KEY_BANK_CODE, KEY_BANK_NAME, KEY_CURRENCY_CODE, KEY_CURRENCY_NAME,
                KEY_BUY_TCHEQUE, KEY_BUY_FCHEQUE, KEY_BUY_TCARD, KEY_BUY_TT, KEY_BUY_CASH, KEY_BUY_DRAFT, KEY_SELL_TCHEQUE,
                KEY_SELL_FCHEQUE, KEY_SELL_TCARD, KEY_SELL_TT, KEY_SELL_CASH, KEY_SELL_DRAFT, KEY_UPDATE_DATE},
                KEY_CURRENCY_CODE + "= '" + currency_code + "'",
                null, null, null, null);
	}

    public List<Currency> getAllCurrencyList(){
        Cursor cursor = getAllCurrency();
        if(cursor == null || cursor.getCount() ==0)
            return null;
        List<Currency> list = new ArrayList<Currency>();
        if(cursor.moveToFirst()){
            list.add(getCurrency(cursor));
            while(cursor.moveToNext()){
                list.add(getCurrency(cursor));
            }
        }
        return list;
    }

    private Currency getCurrency(Cursor cursor){
        if(cursor == null || cursor.getColumnCount() == 0 || cursor.getColumnCount() < 2)
            return null;
        return new Currency(cursor.getString(0), cursor.getString(1));
    }

    public List<Rate> getExRatesList(){
        return getExRateList(getAllRates());
    }

	public List<Rate> getExRatesList(String currency_code){
		return getExRateList(getExchangeRate(currency_code));
	}

	private List<Rate> getExRateList(Cursor cursor){
		if(cursor == null || cursor.getCount() ==0)
			return null;
		List<Rate> list = new ArrayList<Rate>();
        if(cursor.moveToFirst()){
            list.add(getRate(cursor));
            while(cursor.moveToNext()){
                list.add(getRate(cursor));
            }
        }
		return list;
	}

	private Rate getRate(Cursor cursor){
        if(cursor == null || cursor.getColumnCount() == 0 || cursor.getColumnCount() < 16)
            return null;
		return new Rate(BankCode.valueOf(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getDouble(4), cursor.getDouble(5), cursor.getDouble(6), cursor.getDouble(7), cursor.getDouble(7), cursor.getDouble(8), cursor.getDouble(9),
                cursor.getDouble(10), cursor.getDouble(11), cursor.getDouble(12), cursor.getDouble(13), cursor.getDouble(14), new Date(cursor.getLong(15)));
	}
	
	private boolean deleteBankRate(BankCode code){
        return  mDb.delete(DATABASE_TABLE_EXCHANGE_RATES, KEY_BANK_CODE + "= '" + code.name()+"'", null) > 0;
    }
	
    private boolean clearBankRates(){
        return  mDb.delete(DATABASE_TABLE_EXCHANGE_RATES, null, null) > 0;
    }

    public Date lastUpdateDate(){
        Cursor cursor = mDb.rawQuery("SELECT MAX (" + KEY_UPDATE_DATE + ") from " + DATABASE_TABLE_EXCHANGE_RATES, null);
        if(cursor == null || cursor.getCount()==0){
            return null;
        }
        cursor.moveToFirst();
        long lastUpdateDate = cursor.getLong(0);
        if(lastUpdateDate == 0)
            return null;
        return new Date(cursor.getLong(0));
    }

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
            for(String table : TABLES_CREATE){
                db.execSQL(table);
            }
            for(String index : INDEXES_CREATE){
                db.execSQL(index);
            }
            for(String data : DATA_PRELOAD){
                db.execSQL(data);
            }
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { // Not used, but you could upgrade the database with ALTER
			// Scripts
		}
	}
}
