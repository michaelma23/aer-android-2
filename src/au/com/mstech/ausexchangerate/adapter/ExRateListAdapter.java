package au.com.mstech.ausexchangerate.adapter;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import au.com.mstech.ausexchangerate.R;
import au.com.mstech.ausexchangerate.model.BankCode;
import au.com.mstech.ausexchangerate.model.Rate;

public class ExRateListAdapter extends ArrayAdapter<Rate> {
	private Context context; 
	private int layoutResourceId;    
	private List<Rate> data = null;
	private String currencyCode;
	private String rateType;
    private NumberFormat numberFormat;
    
    public ExRateListAdapter(Context context, int layoutResourceId, List<Rate> data,
    		String currencyCode, String rateType) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.setData(data, rateType);
        this.currencyCode = currencyCode;
        numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(4);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ExRateHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new ExRateHolder();
            holder.bankIcon = (ImageView)row.findViewById(au.com.mstech.ausexchangerate.R.id.bankIcon);
            holder.buyRateTxt = (TextView)row.findViewById(au.com.mstech.ausexchangerate.R.id.buyRate);
            holder.sellRateTxt = (TextView)row.findViewById(au.com.mstech.ausexchangerate.R.id.sellRate);
            
            row.setTag(holder);
        }
        else
        {
            holder = (ExRateHolder)row.getTag();
        }
        
        Rate rate = getData().get(position);

        if(rate != null){
            holder.bankIcon.setImageResource(logoIdFromBankCode(rate.getBankCode()));
	        if(rateType.equalsIgnoreCase("Cash")){
	        	setRate(holder, rate.getBuyCash(), rate.getSellCash());
	        }
	        else if(rateType.equalsIgnoreCase("Telegraphic Transfer")){
	        	setRate(holder, rate.getBuyTT(), rate.getSellTT());
	        }
	        else if(rateType.equalsIgnoreCase("Travel Cheque")){
	        	setRate(holder, rate.getBuyTcheque(), rate.getSellTcheque());
	        }
	        else if(rateType.equalsIgnoreCase("Travel Card")){
	        	setRate(holder, rate.getBuyTcard(), rate.getSellTcard());
	        }
	        else if(rateType.equalsIgnoreCase("Foreign Cheque")){
                setRate(holder, rate.getBuyFcheque(), rate.getSellFcheque());
            }
            else if(rateType.equalsIgnoreCase("Draft")){
                setRate(holder, rate.getBuyDraft(), rate.getSellDraft());
            }
        }
        else{
        	setRate(holder, 0, 0);
        }
        
        return row;
    }
    
    private void setRate(ExRateHolder holder, double buy, double sell){

    	if(buy == 0){
    		holder.buyRateTxt.setText("N/A");
    	}
    	else{
    		holder.buyRateTxt.setText(numberFormat.format(buy));
    	}
    	if(sell == 0){
    		holder.sellRateTxt.setText("N/A");
    	}
    	else{
    		holder.sellRateTxt.setText(numberFormat.format(sell));
    	}
    }

    @Override
    public boolean areAllItemsEnabled(){
            return false;
    }

    @Override
    public boolean isEnabled(int position){
            return false;
    }

    public List<Rate> getData() {
        return data;
    }

    public void setData(List<Rate> data, String rateType) {
        if(data != null && rateType != null){
            this.rateType = rateType;
            this.data = new ArrayList<Rate>();
            for(Rate rate : data){
                double sellRate = 0;
                double buyRate = 0;
                if(rateType.equalsIgnoreCase("Cash")){
                    sellRate = rate.getSellCash();
                    buyRate = rate.getBuyCash();
                }
                else if(rateType.equalsIgnoreCase("Telegraphic Transfer")){
                    sellRate = rate.getSellTT();
                    buyRate = rate.getBuyTT();
                }
                else if(rateType.equalsIgnoreCase("Travel Cheque")){
                    sellRate = rate.getSellTcheque();
                    buyRate = rate.getBuyTcheque();
                }
                else if(rateType.equalsIgnoreCase("Travel Card")){
                    sellRate = rate.getSellTcard();
                    buyRate = rate.getBuyTcard();
                }
                else if(rateType.equalsIgnoreCase("Foreign Cheque")){
                    sellRate = rate.getSellFcheque();
                    buyRate = rate.getBuyFcheque();
                }
                else if(rateType.equalsIgnoreCase("Draft")){
                    sellRate = rate.getSellDraft();
                    buyRate = rate.getBuyDraft();
                }
                if(sellRate != 0 || buyRate != 0){
                    this.data.add(rate);
                }
            }
        }
        else{
            this.data = null;
            this.rateType = null;
        }
    }

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getRateType() {
		return rateType;
	}
//
//	public void setRateType(String rateType) {
//		this.rateType = rateType;
//	}

    public int getCount() {
        if(getData() == null){
            return 0;
        }
        return this.getData().size();
    }

    private int logoIdFromBankCode(BankCode code){
        switch (code){
            case ANZ:
                return R.drawable.anz_logo;
            case BWA:
                return R.drawable.bwa_logo;
            case CBA:
                return R.drawable.cba_logo;
            case HSBC:
                return R.drawable.hsbc_logo;
            case NAB:
                return R.drawable.nab_logo;
            case STG:
                return R.drawable.stg_logo;
            case WBC:
                return R.drawable.wbc_logo;
            default:
                return -1;
        }
    }

    static class ExRateHolder{
    	ImageView bankIcon;
        TextView buyRateTxt;
        TextView sellRateTxt;
    }
}
